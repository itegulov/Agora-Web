This project is created using play framework 2.5 seeds template.

<b>For run the project<b> 

You need to download and install sbt for this application to run.

Once you have sbt installed, the following at the command prompt will start up Play in development mode:

sbt run

Play will start up on the HTTP port at http://localhost:9000/. You don't need to deploy or reload anything -- changing any source code while the server is running will 
automatically recompile and hot-reload the application on the next HTTP request.

You can check the hosted version from following url

https://fathomless-taiga-85734.herokuapp.com
